package org.liky.mario;

import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements KeyListener,Runnable {

	
	private List<BackGround> allBG=new ArrayList<BackGround>();
	private BackGround nowBG=null;
	private Mario mario=null;
	private Thread t=new Thread(this);
	private boolean isStart=false;
	
	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		BufferedImage image=new BufferedImage(900,600,BufferedImage.TYPE_3BYTE_BGR);
		Graphics g2=image.getGraphics();
		
		if(isStart){
			//绘制背景
			g2.drawImage(this.nowBG.getBgImage(),0,0,this);	
			
			g2.drawString("life："+this.mario.getLife(),700,60);
			//绘制敌人
			Iterator<Enemy> iterEnemy=this.nowBG.getAllEnemy().iterator();
			while(iterEnemy.hasNext()){
				Enemy en=iterEnemy.next();
				g2.drawImage(en.getShowImage(),en.getX(),en.getY(),this);
			}
			//绘制障碍物
			Iterator<Obstruction> iter=this.nowBG.getAllObstruction().iterator();
			while(iter.hasNext()){
				Obstruction ob=iter.next();
				g2.drawImage(ob.getShowImage(),ob.getX(),ob.getY(),this);
			}
			
			//绘制mario
			g2.drawImage(this.mario.getShowImage(),mario.getX(),mario.getY(),this);
			
		}else{
			g2.drawImage(StaticValue.startImage,0,0,this);
		}
		
		

		
		

		
		//把缓冲图片绘制到窗体中
		g.drawImage(image,0,0,this);
		
	}
	


	public MyFrame(){
		this.setTitle("Mario");
		this.setSize(900,600);
		//得到屏幕大小，设置开始出现位置
		int width=Toolkit.getDefaultToolkit().getScreenSize().width;
		int height=Toolkit.getDefaultToolkit().getScreenSize().height;
		this.setLocation((width-100)/2,(height-700)/2);
		//不可改变屏幕大小
		this.setResizable(false);
		this.addKeyListener(this);
		
		
		
		//初始化图片
		StaticValue.init();
		
		//创建全部场景
		for(int i=1;i<=3;i++){
			this.allBG.add(new BackGround(i,i==3?true:false));
		}
		//将1个场景设置为当前场景
		this.nowBG=this.allBG.get(0);
		
		mario=new Mario(0,600-2*60);	
		this.mario.setBg(nowBG);
		
		t.start();
		this.repaint();
		
		
		//关闭后结束
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
		
	}
	
	public void run(){
		while(true){
			this.repaint();
			try {
				Thread.sleep(50);
				if(this.mario.getX()>=840){
					this.nowBG=this.allBG.get(this.nowBG.getSort());
					this.mario.setBg(this.nowBG);
					this.nowBG.enemyStartMove();
					this.mario.setX(0);
				}
				
				if(this.mario.isDead()){
					JOptionPane.showMessageDialog(this,"Game over");
					System.exit(0);
				}
				if(this.mario.isClear()){
					JOptionPane.showMessageDialog(this,"You win");
					System.exit(0);
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args){
		new MyFrame();
	}
	
	
	@Override
	//当点击键盘上的键时 
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
//		//返回键盘上的值
//		System.out.println(e.getKeyChar());
//		//返回键盘上字符的编码值 左37 右39 空格32
//		System.out.println(e.getKeyCode());
		//当按下39时向右移动->
		if(isStart){		
			if(e.getKeyCode()==39){
				this.mario.rightMove();
			}
			//当按下<-时向左移动
			if(e.getKeyCode()==37){
				this.mario.leftMove();
			}
			//当按下空格时
			if(e.getKeyCode()==32){
				this.mario.jump();
			}
		}else{
			if(e.getKeyCode()==32){
				isStart=true;
				this.nowBG.enemyStartMove();
			}
		}
		
	}

	@Override
	//当抬起键时
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
		//当抬起39时向右停止->
		if(e.getKeyCode()==39){
			this.mario.rightStop();
		}
		//当抬起<-时向左停止
		if(e.getKeyCode()==37){
			this.mario.leftStop();
			this.mario.setLife(3);
			this.mario.setScore(0);
		}
	}

	@Override
	//输入一些信息时
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
